import React from 'react'
import {createRoot} from 'react-dom/client'
import "@arco-design/web-react/dist/css/arco.css"
import router from "./tools/router";
import {RouterProvider} from 'react-router-dom'

const container = document.getElementById('root')

const root = createRoot(container)

root.render(
    <React.StrictMode>
        <RouterProvider router={router}/>
    </React.StrictMode>
)
