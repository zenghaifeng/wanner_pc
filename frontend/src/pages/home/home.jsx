import { Avatar, Input } from "@arco-design/web-react";
import { useRef } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Home = () => {
    const nav = useNavigate();
    const cmd = useRef();
    const runCmd = () => {
        cmd.current.blur();
        nav(cmd.current.dom.value)
        //console.log(cmd.current.dom)
        //console.log(e)
    } 
    return(
        <>
            <div>
                <div>
                    <Input onPressEnter={runCmd} ref={cmd} style={{width:350}} placeholder="请输入命令"/>
                    <Avatar>T</Avatar>
                </div>
                <Outlet />
            </div>
        </>
    )
}
export default Home;
