import { Button, Checkbox, Form, Input } from "@arco-design/web-react";
import { ajax } from "../../tools/ajax";

const Login = () => {
    const [form] = Form.useForm();

    const login = async() => {
        let user = {
            username:form.getFieldValue('username'),
            password: form.getFieldValue('password')
        }

        let result = await ajax("POST",'/login',user)
        console.log(result)
        localStorage.setItem('token',result.data.token)
        localStorage.setItem('refreshtoken',result.data.refreshtoken)
        let r = await ajax("GET","/users")
        console.log(r)
        
    }
    return(
        <>
            <div>
                <Form
                    form={form}
                    onSubmit={login}
                >
                    <Form.Item label='用户名' field='username'>
                        <Input style={{ width: 350 }}/>
                    </Form.Item>
                    <Form.Item label='密码' field='password'>
                        <Input.Password  style={{ width: 350 }}/>
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 5 }}>
                        <Checkbox>I have read manual</Checkbox>
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 5 }}>
                        <Button type="primary" htmlType='submit'>登录</Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    )
}
export default Login;