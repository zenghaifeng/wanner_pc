import { useEffect, useState } from "react";
import { ajax } from "../../tools/ajax";

const Register = () => {
    const [date,setData] = useState();
    useEffect(()=> {
        let result =  async() => {
           const res = await ajax("GET","/users")
           console.log(res.data)
           setData(res.status)
        }
        result();

    },[])
    return(
        <>
            <div>register page</div>
            <div>{date}</div>
        </>
    )
}
export default Register;