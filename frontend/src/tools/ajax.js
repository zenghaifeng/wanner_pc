import axios from "axios"



const baseUrl="http://127.0.0.1:8000";

const ajaxs = axios.create({
    baseURL:baseUrl,
    timeout:5000,
    headers:{
        Authorization:`Bearer ${localStorage.getItem('token')}`
    }
})

//axios.defaults.headers.head.Authorization = localStorage.getItem('token');
// 如果需要跨域，可以设置 withCredentials 为 true
//axios.defaults.withCredentials = true // 允许跨域请求发送 cookies

// 创建 axios 实例
//const service = axios.create({
//    timeout:10000
//})

// 请求拦截器
// ajaxs.interceptors.request.use(config => {
//     const token = 'Bearer '+localStorage.getItem('token') // store.state.token
//     token && (config.headers.Authorization = token);
//     return config;
//     },
//     error => {
//         return Promise.reject(error);

// })
// 响应拦截器
ajaxs.interceptors.response.use(
    response => {
        return Promise.resolve(response)
}, error => {
    return Promise.reject(error);
})



export const ajax = (method="GET",url,data={}) => {
    if (method==="GET") {
        //console.log("geting......")
        return new Promise((resolve,reject) => {
            ajaxs.get(url,{params:data}).then(res => {resolve(res.data);})
            .catch(er => {reject(er.data)})
        })
    }else{
        //console.log("posting......")
        return new Promise((resolve,reject) => {
            ajaxs.post(url,data).then(res => {resolve(res.data)})
            .catch(err => {reject(err.data)})
        })
    }
}