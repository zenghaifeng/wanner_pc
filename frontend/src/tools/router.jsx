import { createBrowserRouter } from "react-router-dom";
import Home from "../pages/home/home";
import Login from "../pages/login/login";
import Register from "../pages/register/register";

const router = createBrowserRouter([
    {
        path:"/",
        element: <Home />,
        children:[
            {
                path:"/login",
                element: <Login />
            },{
                path:"/register",
                element:<Register />
            }
        ],
    },
])

export default router;